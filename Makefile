help:
	echo "make install|uninstall"

install:
	install -o root -m0500 ./bin/sibhoster-virus-checker /usr/bin
	install -o root -m0500 ./bin/sibhoster-virus-log2rss /usr/bin

uninstall:
	rm /usr/bin/sibhoster-virus-checker
	rm /usr/bin/sibhoster-virus-log2rss

.PHONY: help install uninstall
